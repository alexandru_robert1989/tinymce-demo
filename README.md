# TinymceDemo

This projects exemplifies the simple implementation of the
[TinyMCE](https://www.tiny.cloud/ "TinyMCE's Homepage") text editor. 

[TinyMCE](https://www.tiny.cloud/ "TinyMCE's Homepage") is a WYSIWYG (What You See IsWhat You Get)
editor. It allows a user to create a personalised document using a friendly user interface. 
In the background, TinyMCE creates the HTML and the CSS content corresponding to the content 
displayed by the document. The fascinating part is that because in the background TinyMCE 
creates HTML and CSS content, you can use this text editor to not only create a webpage,
but also to easily modify it using a friendly user interface.

This project exemplifies the use of TinyMce in two contexts : as an independent text editor and as
a text editor integrated within an Angular form. The second utilisation is more likely to suit your needs, as it can fit a real 
life scenario, where you can actually use the text editor's content to save it somewhere.

In both scenarios you will find that the text editor has a customized menu. 

## Generating the project

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.0.3 using  the command
 
`ng new tinymce-demo`

## TinyMCE implementation 

After having several strategies for implementing [TinyMCE](https://www.tiny.cloud/ "TinyMCE's Homepage")
within a Angular project, I have chosen the one I found to be the simplest. I simply added a firebase 
TinyMce script within the head of the project's index.html file :

The added script is : 

`<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.10.2/tinymce.min.js" integrity="sha512-MbhLUiUv8Qel+cWFyUG0fMC8/g9r+GULWRZ0axljv3hJhU6/0B3NoL6xvnJPTYZzNqCQU3+TzRVxhkE531CLKg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>`

The script was downloaded from this link : 

[TinyMCE firecloud scripts](https://cdnjs.com/libraries/tinymce)

## Creating an instance of TinyMCE

The logic used for creating a TinyMce instance using a textarea tag is found inside the TinyMceService:

_src/app/core/tinymce.service.ts_

TinyMCE is used within the _SimpleTinyMceDemo_ and the  _FormTinyMceDemo_ components.

## Testing the project 
To test the project first run 

`npm install`

then 

`ng serve`





## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
