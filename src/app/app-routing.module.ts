import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SimpleTinymceDemoComponent} from './shared/components/simpletinymcedemo/simple-tinymce-demo.component';
import {FormTinyMceDemoComponent} from './shared/components/form-tiny-mce-demo/form-tiny-mce-demo.component';

const routes: Routes = [
  {path: 'simple-tiny-mce', component: SimpleTinymceDemoComponent},
  {path: 'app-form-tiny-mce-demo', component: FormTinyMceDemoComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
