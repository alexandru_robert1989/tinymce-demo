import {Injectable} from '@angular/core';

declare var tinymce: any;

@Injectable({
  providedIn: 'root'
})
export class TinymceService {

  constructor() {
  }


  /*
  Function instantiating a customised TinyMCE textarea instance, identified by its id.
   */
  initEditor(id: string): any {

    tinymce.init({
      selector: '#' + id,

      toolbar_location: 'top',
      toolbar_align: 'left',
      resizing: 'true',
      resize_horizontal: 'true',

      plugins: ' save autosave preview print paste ' +
        'code fullscreen fullpage ' +
        'image link media  table template codesample hr charmap emoticons advlist lists insertdatetime ' +
        ' pagebreak nonbreaking anchor toc wordcount ' +
        ' searchreplace  help ',

      toolbar: 'undo redo|preview  fullscreen|styleselect|fontselect|forecolor backcolor|bold italic underline strikethrough|' +
        'alignleft aligncenter alignright alignjustify|charmap emoticons| numlist bullist table hr|link|outdent indent|searchreplace toc',

      menu: {
        file: {title: 'File', items: 'save newdocument restoredraft | preview | print '},
        edit: {title: 'Edit', items: 'undo redo | cut copy paste | selectall   '},
        view: {title: 'View', items: 'code | visualaid visualchars visualblocks | preview fullscreen | fullpage'},
        insert: {
          title: 'Insert',
          items: 'image link media | hr charmap emoticons | toc  pagebreak nonbreaking anchor | template codesample | insertdatetime'
        },
        format: {
          title: 'Format',
          items: 'bold italic underline strikethrough superscript subscript codeformat | formats blockformats fontformats fontsizes align | forecolor backcolor | removeformat'
        },
        tools: {title: 'Tools', items: 'wordcount searchreplace code'},
        table: {title: 'Table', items: 'inserttable | cell row column | tableprops deletetable'},
        help: {title: 'Help', items: 'help'}
      },


      /*
        file_picker_callback(callback, value, meta): any {
              // Provide file and text for the link dialog
              if (meta.filetype === 'file') {
                callback('mypage.html', {text: 'My text'});
              }

              // Provide image and alt text for the image dialog
              if (meta.filetype === 'image') {
                callback('myimage.jpg', {alt: 'My alt text'});
              }

              // Provide alternative source and posted for the media dialog
              if (meta.filetype === 'media') {
                callback('movie.mp4', {source2: 'alt.ogg', poster: 'image.jpg'});
              }
            },
       */


      a11y_advanced_options: true,
      block_unsupported_drop: false,
      file_picker_types: 'file image media',
      image_caption: true,
      image_advtab: true,
      image_title: true,
      image_uploadtab: true,
      image_prepend_url: 'assets/images/',
      automatic_uploads: true,
      images_upload_url: 'postAcceptor.php',
      images_upload_base_path: '/assets/images',
      paste_data_images: true
    });
  }


  /*
  If the textarea iframe was not loaded correctly, the iframe will have a lenght > 0.
  In this case you should reload the page, thus triggering the textarea reload also.
   */
  reloadEditor(): any {
    const iframe = document.getElementsByTagName('iframe');
    if (iframe.length > 0) {
      window.location.reload();
    }
  }
}
