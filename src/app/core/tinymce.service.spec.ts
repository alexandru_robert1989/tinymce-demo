import { TestBed } from '@angular/core/testing';

import { TinymceService } from './tinymce.service';

describe('TinymceService', () => {
  let service: TinymceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TinymceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
