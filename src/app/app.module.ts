import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SimpleTinymceDemoComponent } from './shared/components/simpletinymcedemo/simple-tinymce-demo.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NavbarComponent} from './shared/components/navbar/navbar.component';
import { FormTinyMceDemoComponent } from './shared/components/form-tiny-mce-demo/form-tiny-mce-demo.component';
import { FooterComponent } from './shared/components/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    SimpleTinymceDemoComponent,
    NavbarComponent,
    FormTinyMceDemoComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
