import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleTinymceDemoComponent } from './simple-tinymce-demo.component';

describe('SimpletinymcedemoComponent', () => {
  let component: SimpleTinymceDemoComponent;
  let fixture: ComponentFixture<SimpleTinymceDemoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SimpleTinymceDemoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleTinymceDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
