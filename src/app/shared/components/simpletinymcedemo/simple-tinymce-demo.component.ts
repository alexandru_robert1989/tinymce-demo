import {Component, OnInit} from '@angular/core';
import {TinymceService} from '../../../core/tinymce.service';

@Component({
  selector: 'app-simpletinymcedemo',
  templateUrl: './simple-tinymce-demo.component.html',
  styleUrls: ['./simple-tinymce-demo.component.css']
})
export class SimpleTinymceDemoComponent implements OnInit {

  constructor(private tinyMceService: TinymceService) {
  }


  ngOnInit() {
    this.tinyMceService.initEditor('mymce1');
    this.tinyMceService.reloadEditor();
  }

}
