import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTinyMceDemoComponent } from './form-tiny-mce-demo.component';

describe('FormTinyMceDemoComponent', () => {
  let component: FormTinyMceDemoComponent;
  let fixture: ComponentFixture<FormTinyMceDemoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormTinyMceDemoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTinyMceDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
