import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {TinymceService} from '../../../core/tinymce.service';

declare var tinymce: any;

@Component({
  selector: 'app-form-tiny-mce-demo',
  templateUrl: './form-tiny-mce-demo.component.html',
  styleUrls: ['./form-tiny-mce-demo.component.css']
})
export class FormTinyMceDemoComponent implements OnInit {

  tinyMceForm: any;

  //value used if you want to modify existing data
  defaultContent = 'default content value';

  htmlContent = '';
  rawContent = '';


  constructor(private formBuilder: FormBuilder,
              private tinyMceService: TinymceService) {
  }

  ngOnInit(): void {

    //add logic to retrieve real content using a subscription
 //   this.defaultContent = 'updated default content';
    this.updateDefaultContent();

    //instantiate the textarea and the form once the textarea vas retrieved
    this.tinyMceService.initEditor('demotiny');
    this.tinyMceService.reloadEditor();
    this.tinyMceForm = this.formBuilder.group({
      content: [this.defaultContent]
    });


  }

  reloadPage(): any {
    const iframe = document.getElementsByTagName('iframe');
    if (iframe.length > 0) {
      window.location.reload();
    }
  }

  onFormSubmit(): any {

    //retrieve the editor's html and raw content and use it as needed
    this.htmlContent = tinymce.get('demotiny').getContent();
    this.rawContent = tinymce.get('demotiny').getBody().textContent;

    //open your navigator's console using 'ctrl  shift j' to check the retrieved content
    console.log('Form submited. HTML value : ', this.htmlContent, '\nRaw content : ', this.rawContent);

  }

  updateDefaultContent() {
    this.defaultContent = '<html>\n' +
      '<head>\n' +
      '</head>\n' +
      '<body>\n' +
      '<h1 style="text-align: center;"><span style="color: #3598db;">updated default content</span></h1>\n' +
      '</body>\n' +
      '</html>';
  }

}
