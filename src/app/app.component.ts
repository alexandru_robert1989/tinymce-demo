import {Component} from '@angular/core';
import {TinymceService} from './core/tinymce.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'tinymce-demo';


  constructor(private tinyMceService: TinymceService,
              private router: Router) {
  }




}
